import chai from 'chai'
chai.should()

class World extends Array {
	static get [Symbol.species]() { return Array; }

	addCell(cell) {
		const preWorld = [...this]
		preWorld.push(cell)
		return new World(...preWorld)
	}

	iterate() {
		return new World(new Cell(new Coordinate(0), new Coordinate(0), Cell.Dead))
	}

	getCell(x, y) {
		return this.filter(cell => cell.x.equal(x) && cell.y.equal(y))[0]
	}

	getLivingNeighbourCount(cell) {
		return this
			.filter(({ x, y }) => !cell.x.equal(x) && !cell.x.equal(y))
			.filter(({x}) => cell.x.equal(x - 1) || cell.x.equal(x) || cell.x.equal(x + 1))
			.filter(({y}) => cell.y.equal(y - 1) || cell.y.equal(y) || cell.y.equal(y + 1))
			.length
	}

}


class Coordinate {
	constructor(number) {
		this.number = number
	}


	equal(coordinate) {
		return this.number === coordinate.number
	}
}


class Cell {
	constructor(x, y, state) {
		this.x = x
		this.y = y
		this.state = state
	}
}

Cell.Alive = true
Cell.Dead = false



describe('Game of life', () => {
	it('should compare coordinate', () => {
		const x1 = new Coordinate(0)
		const x2 = new Coordinate(0);
		// (x1 == x2).should.be.equal(true)
		x1.should.be.deep.equal(x2)
	});


	it('should add a cell in the world', () => {
		const cell = new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive)
		const world = new World()

		const world_with_a_cell = world.addCell(cell)

		world_with_a_cell[0].should.be.equal(cell)
	})


	it('should retrieve the cell', () => {
		const cell = new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive)
		const world = new World()
		const world_with_a_cell = world.addCell(cell)

		const newCell = world_with_a_cell.getCell(new Coordinate(0), new Coordinate(0))

		newCell.should.be.equal(cell)
	})



	it('should count the neighbour', () => {
		const world = new World(
			new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive),
			new Cell(new Coordinate(0), new Coordinate(1), Cell.Alive),
			new Cell(new Coordinate(1), new Coordinate(0), Cell.Alive),
		)

		world.getLivingNeighbourCount(new Cell(new Coordinate(0), new Coordinate(0)))
	})


	describe('should iterate', () => {
		it('with 1 living cell', () => {
			const cell = new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive)
			const world = new World()
			const world_with_a_cell = world.addCell(cell)

			const next_generation = world_with_a_cell.iterate()

			const newCell = next_generation.getCell(new Coordinate(0), new Coordinate(0))
			newCell.should.be.deep.equal(new Cell(new Coordinate(0), new Coordinate(0), Cell.Dead))
		})


		it('with 3 living cells', () => {
			const world = new World(
				new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive),
				new Cell(new Coordinate(0), new Coordinate(1), Cell.Alive),
				new Cell(new Coordinate(1), new Coordinate(0), Cell.Alive),
			)

			const next_generation = world.iterate()

			const newCell = next_generation.getCell(new Coordinate(0), new Coordinate(0))
			newCell.should.be.deep.equal(new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive))
			newCell.should.be.deep.equal(new Cell(new Coordinate(0), new Coordinate(1), Cell.Dead))
			newCell.should.be.deep.equal(new Cell(new Coordinate(1), new Coordinate(0), Cell.Dead))
		})


		it.skip('osef', () => {
			const world = new World(
				new Cell(new Coordinate(0), new Coordinate(0), Cell.Alive),
				new Cell(new Coordinate(0), new Coordinate(1), Cell.Alive),
				new Cell(new Coordinate(1), new Coordinate(0), Cell.Alive),
			)

			const next_generation = world.iterate()

			const newCell = next_generation.getCell(new Coordinate(0), new Coordinate(0))
			newCell.should.be.deep.equal(new Cell(new Coordinate(0), new Coordinate(0), Cell.Dead))
			newCell.should.be.deep.equal(new Cell(new Coordinate(0), new Coordinate(1), Cell.Dead))
			newCell.should.be.deep.equal(new Cell(new Coordinate(1), new Coordinate(0), Cell.Dead))
			newCell.should.be.deep.equal(new Cell(new Coordinate(1), new Coordinate(1), Cell.Alive))
		})
	})
});
